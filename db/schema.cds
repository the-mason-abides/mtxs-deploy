using { cuid } from '@sap/cds/common';

entity Orders: cuid {
  orderNumber: Integer;
  orderTotal: Decimal;
}