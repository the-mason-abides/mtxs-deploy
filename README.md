# Overview

This is an extremely simple CAP multi-tenant project to duplicate a subscription issue happening in cloud foundry.

It was built using the guidelines shown in the Multi-tenant Deployment Cookbook.


## Steps to Recreate

- Use Cloud Foundry CLI to login and set target to CF space that is intended for deployment
- Call npm script (see package) **deploy** (command **npm run deploy**)
- Use BTP CLI to login and set target to consumer sub account that you intend to subscribe multi-tenant application to
- Use BTP CLI command **btp --verbose subscribe accounts/subaccount --to-app <multi-tenant-app-name>** where <multi-tenant-app-name> is the name of the newly dployed app

## Results

The subscription is regularly failing with an **HDI Make Failed** message within the **mtxs-deploy-mtx** sidecar app
