using { Orders as dbOrders } from '../db/schema';

@path: 'service/order'
service OrderService {
  entity Orders as projection on dbOrders;
}